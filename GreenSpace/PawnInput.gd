class_name PawnInput
extends Node



var direction:=Vector3.ZERO
var normalized:=Vector3.ZERO


var rotation:=Vector2.ZERO


func updateForLocal():
	direction = Vector3.ZERO
	direction += Input.get_action_strength("forward") * Vector3.FORWARD
	direction += Input.get_action_strength("back") * Vector3.BACK
	direction += Input.get_action_strength("left") * Vector3.LEFT
	direction += Input.get_action_strength("right") * Vector3.RIGHT
	
	rotation = Vector2.ZERO
	rotation.x -= Input.get_action_strength("lookLeft")
	rotation.x += Input.get_action_strength("lookRight")
	print(Input.get_action_strength("lookLeft"))
	
	
	normalized = direction.normalized()
	return self
